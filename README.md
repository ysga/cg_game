# CG_Game

Tested with open-jdk-11 (on Ubuntu 20.04 & Windows 10)

- to run CG_Game
`$ java -jar "path to CGGame.jar"`

- use "true" command line parameter to see collision rectangles.
`$ java -jar "path to CGGame.jar" true`
